import express from 'express';
export const router = express.Router();
import bodyParser from 'body-parser';

router.get('/', (req, res) => {
  const params = {
    numContrato: req.query.numContrato,
    nombre: req.query.nombre,
    domicilio: req.query.domicilio,
    estudios: req.query.estudios,
    pagoDiario: req.query.pagoDiario,
    dias: req.query.dias,
  };

  res.render('principal.html', params);
});

router.post('/', (req, res) => {
  const params = {
    numContrato: req.body.numContrato,
    nombre: req.body.nombre,
    domicilio: req.body.domicilio,
    estudios: req.body.estudios,
    pagoDiario: req.body.pagoDiario,
    dias: req.body.dias,
  };

  res.render('resultado.html', params);
});

router.get('/contactos', (req, res) => {
  res.render('contactos.html');
});

router.get('/empresa', (req, res) => {
  res.render('empresa.html');
});
